import '../styles/index.scss'

window.addEventListener('load', async () => {
  await new Promise((r) => setTimeout(r, 1000))

  const fileInput = document.querySelector('#file-input')
  const fileInputLabel = document.querySelector('label[for="file-input"]')
  const iframe = document.querySelector('#image-frame')

  const handleFileChange = ({ target }) => {
    const { files } = target
    const { name } = files['0']
    console.log(files['0'])
    if (files['0'].type.split('/')[0] !== 'image') {
      alert('Invalid file format. Please, upload an image')
      return false
    }

    if (files['0'] instanceof File) {
      const reader = new FileReader()
      reader.readAsDataURL(files['0'])
      reader.onload = ({ target: { result } }) => {
        fileInputLabel.innerHTML = name
        const image = iframe.contentDocument.querySelector('img')
        image.src = result
        image.alt = name
      }
    }
  }

  fileInput.addEventListener('change', handleFileChange)
})
