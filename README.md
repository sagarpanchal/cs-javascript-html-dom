# Setup

## boilerplate

- Includes webpack HMR
- Webpack 4
- Es6 / Babel
- Webpack CSS loader / Style loader

## Install dependencies

```
npm i
```

## Run Development Server

```
npm run dev
```

## Create Production Build

```
npm run build
```
